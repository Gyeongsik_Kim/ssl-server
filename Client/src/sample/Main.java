package sample;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.UnknownHostException;
import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;
import java.util.Scanner;

import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSocket;
import javax.net.ssl.SSLSocketFactory;


public class Main {
    public static void main(String[] args) throws UnknownHostException, IOException, NoSuchAlgorithmException, KeyManagementException {
        int port = 10789;
        String host = "192.168.0.12";

        SSLContext context = SSLContext.getInstance("SSL");
        context.init(null, null, null);
        SSLSocketFactory factory = context.getSocketFactory();
        String[] suites = factory.getSupportedCipherSuites();
        SSLSocket socket = (SSLSocket) factory.createSocket(host, port);
        socket.setEnabledCipherSuites(suites);
        System.out.println(socket.getSession().getCipherSuite());


        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(socket.getInputStream()));
        PrintWriter printWriter = new PrintWriter(socket.getOutputStream(), true);
        Scanner scan = new Scanner(System.in);

        while (true) {
            System.out.print("> ");
            String text = scan.nextLine();

            printWriter.println(text);

            text = bufferedReader.readLine();
            if (text != null) {
                System.out.println("server> " + text);
                if (text.equals("ack")) {
                    text = bufferedReader.readLine();
                    if (text.equals("fin")) {
                        System.out.println("server> " + text);
                        System.out.println("> ack");
                        printWriter.println("ack");
                        break;
                    }
                }
            }
        }
        System.out.println("system Exit");
    }

}
