package Server;import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.security.KeyManagementException;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.UnrecoverableKeyException;
import java.util.Scanner;

import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLServerSocket;
import javax.net.ssl.SSLServerSocketFactory;
import javax.net.ssl.SSLSocket;


public class Main {
    public static void main(String[] args) throws IOException, NoSuchAlgorithmException, KeyStoreException, UnrecoverableKeyException, KeyManagementException {
        int port = 10789;
        Scanner scan = new Scanner(System.in);

        SSLContext context = SSLContext.getInstance("SSL");
        context.init(null, null, null);
        SSLServerSocketFactory factory = context.getServerSocketFactory();

        String [] suites = factory.getSupportedCipherSuites();
        SSLServerSocket serverSocket = (SSLServerSocket) factory.createServerSocket(port);
        serverSocket.setEnabledCipherSuites(suites);

        SSLSocket socket = (SSLSocket) serverSocket.accept();
        System.out.println(socket.getSession().getCipherSuite());
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(socket.getInputStream()));
        PrintWriter printWriter = new PrintWriter(socket.getOutputStream(), true);

        while (true) {

            String text = bufferedReader.readLine();
            if (text != null) {
                System.out.println("client> " + text);
                if (text.equals("fin")) {
                    System.out.println("send ack");
                    text = "ack";
                    printWriter.println(text);
                    System.out.println("send fin");
                    text = "fin";
                } else if (text.equals("ack")) {
                    break;
                }

                printWriter.println(text);
            }

        }
        System.out.println("System exit");
    }
}
