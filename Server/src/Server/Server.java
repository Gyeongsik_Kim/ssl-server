package Server;

import javax.net.ssl.SSLSocket;
import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;

/**
 * Created by GyungDal on 2016-08-05.
 */
public class Server extends Thread {

    private SSLSocket socket;
    private InputStream input;
    private InputStreamReader reader;
    private BufferedReader br;

    public Server(SSLSocket socket){
        this.socket = socket;
    }

    @Override
    public void run() {

        try{
            String fromClient = null;
            input = socket.getInputStream();
            reader = new InputStreamReader(input);
            br = new BufferedReader(reader);

            while((fromClient = br.readLine())!=null){
                System.out.println(fromClient);
                System.out.flush();
            }

        }catch(Exception e){
        }

    }

}
